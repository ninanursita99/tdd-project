from django.shortcuts import render
import datetime

# Enter your name here
mhs_name = 'Nina Nursita Ramadhan' # TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age':calculate_age(1999)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    now = datetime.datetime.now()
    age = now.year - birth_year
    return age
